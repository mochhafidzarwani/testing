#ifndef MultiFlock_h
#define MultiFlock_h

#define DEBUG

#include <Arduino.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <ModbusMaster.h>
#include <Preferences.h>
#include <WiFi.h>

WiFiClient esp;
WiFiClient esp2;
ModbusMaster modbus;

#define MODBUS_DIR_PIN 32 
#define MODBUS_RX_PIN  33 
#define MODBUS_TX_PIN  25 
#define MODBUS_SERIAL_BAUD 9600

struct data
{
    int _realtemp;
    int _temp;
    int _calval;
    int _humid;
    int _lock;
    uint8_t _status;
    int _max;
    int _min;
    bool _maxen;
    bool _minen;
    bool _request;
};
struct topic_pub
{
    String suhu;
    String lembab;
    String notif;
    String synch;
    String log;
    String ver;
    String max;
    String min;
    String maxen;
    String minen;
    String calval;
    String calres;
};
struct topic_sub
{
    String req;
    String max;
    String min;
    String maxen;
    String minen;
    String calval;
    String calres;
};

void modbusPreTransmission() {
  delay(500);
  digitalWrite(MODBUS_DIR_PIN, HIGH);
}
void modbusPostTransmission() {
  digitalWrite(MODBUS_DIR_PIN, LOW);
  delay(500);
}
void modbusSetup() {
    pinMode(MODBUS_DIR_PIN, OUTPUT);
    Serial.begin(115200);
    Serial2.begin(MODBUS_SERIAL_BAUD, SERIAL_8N1, MODBUS_RX_PIN, MODBUS_TX_PIN);
    Serial2.setTimeout(200);
    modbus.start(Serial2);
    modbus.preTransmission(modbusPreTransmission);
    modbus.postTransmission(modbusPostTransmission);
}
 
class Flock : public PubSubClient, private Preferences
{

public:
    char terminalTime[50];
    const char* client_id = "1000110001100011002";
    const char* username  = "iotChickinHMI";
    const char* password  = "6W38KkAZ1%1w";
    data _data;
    topic_pub _pub;
    
    Flock(const char *, ModbusMaster *);
    void setup(Client& client);
    void Read();
    void NotifSend();
    void LogSend();
    void mqttConnect();
    void callback(char *, uint8_t *, unsigned int );

private:
    ModbusMaster *modbus;
    const char *lantai;
    topic_sub _sub;
    uint8_t current_status;
    String _payload;
    unsigned long logMillis;
    void topicProcess();
    void Timing();
    void Compare();
    void TempSend();
    void HumSend();
    void Synchronize();
    
    void callback_max(String);
    void callback_maxen(String);
    void callback_min(String);
    void callback_minen(String);
    void callback_calval(String);
    void callback_calres(String);
    void callback_version(String);
    
};

Flock::Flock(const char *_lantai, ModbusMaster *_modbus)
{
    modbus = _modbus;
    lantai = _lantai;
    logMillis = millis();
}
void Flock::setup(Client& client) {
    topicProcess();
    begin(lantai, false);
    setServer("18.140.62.3", 1883);
    setBufferSize(5000);
    setKeepAlive(60000);
    Serial.println(lantai);
    std::function<void(char*, uint8_t*, unsigned int)> callback = [=](char* topic, uint8_t* payload, unsigned int length) {
    this->callback(topic, payload, length);
    };
    setCallback(callback);
    setClient(client);
}
void Flock::Read() {
    Timing();
    if (!strcmp(lantai, "Lantai1"))
        modbus->setSlaveID(1);
    else if (!strcmp(lantai, "Lantai2"))
        modbus->setSlaveID(2);

    uint8_t result;
    result = modbus->readHoldingRegisters(0, 2);
    if (result == modbus->ku8MBSuccess) {
        _data._realtemp = modbus->getResponseBuffer(0);
        _data._humid    = modbus->getResponseBuffer(1);
#ifdef DEBUG
        Serial.println("Success, Received data: ");
        Serial.printf("%s\n\nTemp: %.1f C\nHumid: %.1f %%\n\n\n", lantai, (float)_data._realtemp / 10, (float)_data._humid / 10);
#endif
    } else {
        _data._realtemp = 0;
        _data._humid = 0;
#ifdef DEBUG
        Serial.print("Failed, Response Code: ");
        Serial.print(result, HEX);
        Serial.println("");
#endif
    }
    _data._temp = _data._realtemp;
    if (_data._calval != 0) {
        int offset;
        if (((int)_data._calval * 10) > _data._lock) {
            offset = (_data._calval * 10) - _data._lock;
            _data._temp = _data._realtemp + offset;
        } 
        if (((int)_data._calval * 10) < _data._lock) {
            offset = _data._lock - (_data._calval * 10);
            _data._temp = _data._realtemp - offset;
        }
    }
    TempSend();
    HumSend();
}
void Flock::TempSend() {
    StaticJsonDocument<100> JSONbuffer;
    JSONbuffer["_terminalTime"] = terminalTime;

    if (lantai == "Lantai1")
        JSONbuffer["_groupName"] = "L1_SENSOR_TEMP";
    else if (lantai == "Lantai2")
        JSONbuffer["_groupName"] = "L2_SENSOR_TEMP";

    JSONbuffer["status"] = (String)_data._temp;
    char payload_temp[100];
    serializeJson(JSONbuffer, payload_temp);
    publish(_pub.suhu.c_str(), payload_temp, true);

#ifdef DEBUG
    Serial.println(payload_temp);
#endif
}
void Flock::HumSend() {
    StaticJsonDocument<100> JSONbuffer;
    JSONbuffer["_terminalTime"] = terminalTime;

    if (lantai == "Lantai1")
        JSONbuffer["_groupName"] = "L1_SENSOR_HUMI";
    else if (lantai == "Lantai2")
        JSONbuffer["_groupName"] = "L2_SENSOR_HUMI";

    JSONbuffer["status"] = (String)_data._humid;
    char payload_hum[100];
    serializeJson(JSONbuffer, payload_hum);
    publish(_pub.lembab.c_str(), payload_hum);

#ifdef DEBUG
    Serial.println(payload_hum);
#endif
}
void Flock::NotifSend() {
    Compare();
    if (current_status != _data._status){
    StaticJsonDocument<100> JSONbuffer;
    JSONbuffer["_terminalTime"] = terminalTime;

    if (lantai == "Lantai1")
        JSONbuffer["_groupName"] = "L1_NOTIF";
    else if (lantai == "Lantai2")
        JSONbuffer["_groupName"] = "L2_NOTIF";

    JSONbuffer["status"] = (String)_data._status;
    JSONbuffer["temp"]   = (String)_data._temp;
    char payload_notif[100];
    serializeJson(JSONbuffer, payload_notif);
    publish(_pub.notif.c_str(), payload_notif);
    current_status = _data._status;
#ifdef DEBUG
    Serial.println(payload_notif);
#endif
    }
}
void Flock::Synchronize() {
    _data._lock     = getInt("lock", 0);
    _data._max      = getUInt("max", 40);
    _data._min      = getUInt("min", 5);
    _data._maxen    = getUInt("maxen", 0);
    _data._minen    = getUInt("minen", 0);
    _data._calval   = getFloat("calval", 0);
    _data._calval   = _data._calval * 10;
    StaticJsonDocument<500> doc;
    doc["_terminalTime"]    = terminalTime;

    if (lantai == "Lantai1")
        doc["_groupName"]   = "L1_SYNCH";
    else if (lantai == "Lantai2")
        doc["_groupName"]   = "L2_SYNCH";

    doc["temp"]             = (String)_data._temp;
    doc["humd"]             = (String)_data._humid;
    doc["cal"]              = (String)_data._calval;
    doc["sn"]               = "SN-CHICKIN";
    doc["version"]          = "1.5.0";
    doc["alarmMinValue"]    = (String)_data._min;
    doc["alarmMinStatus"]   = (String)_data._minen;
    doc["alarmMaxValue"]    = (String)_data._max;
    doc["alarmMaxStatus"]   = (String)_data._maxen;
    char payload_synch[300];
    serializeJson(doc, payload_synch); 
    publish(_pub.synch.c_str(), payload_synch);
    _data._calval = _data._calval / 10;

#ifdef DEBUG
    Serial.println(payload_synch);
#endif
}
void Flock::LogSend() {
    if (millis() - logMillis > 60000) {
    _data._calval = _data._calval * 10;
    StaticJsonDocument<300> JSONbuffer;
    JSONbuffer["_terminalTime"] = terminalTime;

    if (lantai == "Lantai1")
        JSONbuffer["_groupName"]    = "L1_LOG";
    else if (lantai == "Lantai2")
        JSONbuffer["_groupName"]    = "L2_LOG";
    
    JSONbuffer["temp"]          = (String)_data._temp;
    JSONbuffer["humd"]          = (String)_data._humid;
    JSONbuffer["cal"]           = (String)_data._calval;
    JSONbuffer["version"]       = "1.5.0";
    JSONbuffer["productId"]     = "CSESP2023";

    char payload_log[300];
    serializeJson(JSONbuffer, payload_log);
    publish(_pub.log.c_str(), payload_log);
    _data._calval = _data._calval / 10;

#ifdef DEBUG
    Serial.println(payload_log);
#endif
    logMillis = millis();
    }
}
void Flock::Timing() {
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo))
    {
        Serial.println("Failed to obtain time");
    }
    sprintf(terminalTime, "%04d-%02d-%02d %02d:%02d:%02d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
}
void Flock::Compare() {
    if (_data._temp != 0)
    {
        if (current_status == 3)
        {
            _data._status = 4;
        }
        else if (_data._temp > _data._max && _data._maxen)
        {
            _data._status = 1;
        }
        else if (_data._temp < _data._min && _data._minen)
        {
            _data._status = 2;
        }
        else if ((_data._temp < _data._max && _data._temp > _data._min) && (_data._maxen && _data._minen) && current_status != 4)
        {
            _data._status = 5;
        }
    }
    else
    {
        _data._status = 3;
    }
}
void Flock::topicProcess() {
    _pub.suhu   = "data/" + String(client_id) + "/" + String(lantai) + "/SENSOR/TEMP/8883";
    _pub.lembab = "data/" + String(client_id) + "/" + String(lantai) + "/SENSOR/HUMI/8883";
    _pub.max    = "data/" + String(client_id) + "/" + String(lantai) + "/ALARM/MAX/8883";
    _pub.maxen  = "data/" + String(client_id) + "/" + String(lantai) + "/ALARM/MAXEN/8883";
    _pub.min    = "data/" + String(client_id) + "/" + String(lantai) + "/ALARM/MIN/8883";
    _pub.minen  = "data/" + String(client_id) + "/" + String(lantai) + "/ALARM/MINEN/8883";
    _pub.calval = "data/" + String(client_id) + "/" + String(lantai) + "/CALVAL/TEMP/8883";
    _pub.calres = "data/" + String(client_id) + "/" + String(lantai) + "/CALRES/TEMP/8883";
    _pub.ver    = "data/" + String(client_id) + "/VERSION/8883";
    _pub.synch  = "data/" + String(client_id) + "/" + String(lantai) + "/SYNC/8883";
    _pub.log    = "data/" + String(client_id) + "/" + String(lantai) + "/LOG/8883";
    _pub.notif  = "data/" + String(client_id) + "/" + String(lantai) + "/ALARM/NOTIF/8883";

    _sub.max    = "cmd/" + String(client_id) + "/" + String(lantai) + "/ALARM/MAX/8883";
    _sub.maxen  = "cmd/" + String(client_id) + "/" + String(lantai) + "/ALARM/MAXEN/8883";
    _sub.min    = "cmd/" + String(client_id) + "/" + String(lantai) + "/ALARM/MIN/8883";
    _sub.minen  = "cmd/" + String(client_id) + "/" + String(lantai) + "/ALARM/MINEN/8883";
    _sub.calres = "cmd/" + String(client_id) + "/" + String(lantai) + "/CALRES/TEMP/8883";
    _sub.calval = "cmd/" + String(client_id) + "/" + String(lantai) + "/CALVAL/TEMP/8883";
    _sub.req    = "cmd/" + String(client_id) + "/REQ/8883";
}
void Flock::mqttConnect() {
    while (!connected()) {
    #ifdef DEBUG
    Serial.printf("The client %s connects to the MQTT broker\n", client_id);
    #endif
    if (connect(client_id, username, password)) {
        #ifdef DEBUG
        Serial.println("EMQX MQTT broker connected");
        #endif
        configTime(21600, 3600, "pool.ntp.org");

        Synchronize();
        subscribe(_sub.max.c_str());
        subscribe(_sub.maxen.c_str());
        subscribe(_sub.min.c_str());
        subscribe(_sub.minen.c_str());
        subscribe(_sub.calval.c_str());
        subscribe(_sub.calres.c_str());
        subscribe(_sub.req.c_str());
    } else {
        #ifdef DEBUG
        Serial.print("failed with state ");
        Serial.println(state());
        #endif
        delay(1000);
    }       
  }
}
void Flock::callback(char *topic, uint8_t *payload, unsigned int length) {
        Serial.println(strstr(topic, lantai)); 
        _payload = "";
        if (strstr(topic, lantai) != NULL) {
            if (strstr(topic, "/MAX/") != NULL ) {
                for (int i = 0; i < length; i++) {
                _payload += (char)payload[i];
                #ifdef DEBUG
                Serial.print((char)payload[i]);
                #endif
                }
                callback_max(_payload);
            } else if (strstr(topic, "/MIN/") != NULL) {
                for (int i = 0; i < length; i++) {
                _payload += (char)payload[i];
                #ifdef DEBUG
                Serial.print((char)payload[i]);
                #endif
                }
                callback_min(_payload);
            } else if (strstr(topic, "/MAXEN/") != NULL) {
                for (int i = 0; i < length; i++) {
                _payload += (char)payload[i];
                #ifdef DEBUG
                Serial.print((char)payload[i]);
                #endif
                }
                callback_maxen(_payload);
            } else if (strstr(topic, "/MINEN/") != NULL) {
                for (int i = 0; i < length; i++) {
                _payload += (char)payload[i];
                #ifdef DEBUG
                Serial.print((char)payload[i]);
                #endif
                }
                callback_minen(_payload);
            } else if (strstr(topic, "CALRES") != NULL) {
                for (int i = 0; i < length; i++) {
                _payload += (char)payload[i];
                #ifdef DEBUG
                Serial.print((char)payload[i]);
                #endif
                }
                callback_calres(_payload);
            } else if (strstr(topic, "CALVAL") != NULL) {
                for (int i = 0; i < length; i++) {
                _payload += (char)payload[i];
                #ifdef DEBUG
                Serial.print((char)payload[i]);
                #endif
                }
                callback_calval(_payload);
            } 
        } else if (strstr(topic, "REQ") != NULL) {
            for (int i = 0; i < length; i++) {
            _payload += (char)payload[i];
            #ifdef DEBUG
            Serial.print((char)payload[i]);
            #endif
            }
            callback_version(_payload);
        }
}
void Flock::callback_max(String _payload) {
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);

        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif

        const int value = doc["payload"]["status"];       
        putUInt("max", value);             
        _data._max = value;

        #ifdef DEBUG
        Serial.printf("\nMaximum Temp. Threshold : %d\n", value);
        #endif

        doc.clear();
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["status"]        = (String)_data._max ;
        char payload_callback[100];
        serializeJson(doc, payload_callback);            
        publish(_pub.max.c_str(), payload_callback);
    }
void Flock::callback_min(String _payload) {
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);

        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif
        const int   value = doc["payload"]["status"];             
        putUInt("min", value);            
        _data._min = value;

        #ifdef DEBUG
        Serial.printf("\nMinimum Temp. Threshold : %d\n", value);
        #endif
        
        doc.clear();
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["status"]        = (String)_data._min;
        char payload_callback[100];
        serializeJson(doc, payload_callback);           
        publish(_pub.min.c_str(), payload_callback);
    }
void Flock::callback_maxen(String _payload){
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);
        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif
        const int   value = doc["payload"]["status"];              
        putUInt("maxen", value);                   
        _data._maxen = value;

       #ifdef DEBUG
           Serial.printf("\nAlarm Max Status : %d\n", value);
        #endif

        doc.clear();    
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["status"]        = (String)_data._maxen;
        char payload_callback[100];
        serializeJson(doc, payload_callback);           
        publish(_pub.maxen.c_str(), payload_callback);
        
    }
void Flock::callback_minen(String _payload){
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);
        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif
        const int   value = doc["payload"]["status"];       
        putUInt("minen", value);                  
        _data._minen = value;

        #ifdef DEBUG
            Serial.printf("\nAlarm Min Status : %d\n", value);    
        #endif
        
        doc.clear();
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["status"]        = (String)_data._minen ;
        char payload_callback[100];
        serializeJson(doc, payload_callback);           
        publish(_pub.minen.c_str(), payload_callback);
    }
void Flock::callback_calval(String _payload){
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);
        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif
        const float value = doc["payload"]["status"];         
        putFloat("calval", value);     
        _data._calval = value;

        #ifdef DEBUG
            Serial.printf("\nCallibration Value : %d\n", _data._calval);
        #endif
        
        _data._lock = _data._realtemp;                                       
        putInt("lock", _data._lock);

        doc.clear();
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["status"]        = (String)_data._calval ;
        char payload_callback[100];
        serializeJson(doc, payload_callback);           
        publish(_pub.calval.c_str(), payload_callback);       
  
    }
void Flock::callback_calres(String _payload){
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);
        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif
        const float value = doc["payload"]["status"];        
        putFloat("calval", value);                
        _data._calval = value;

        #ifdef DEBUG
          Serial.printf("\nCallibration Value : %d\n", _data._calval);  
        #endif

        doc.clear();
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["status"]        = (String)_data._calval ;
        char payload_callback[100];
        serializeJson(doc, payload_callback);            
        publish(_pub.calres.c_str(), payload_callback); 

    }
void Flock::callback_version(String _payload){
        StaticJsonDocument<300> doc;
        DeserializationError error = deserializeJson(doc, _payload);
        #ifdef DEBUG
        if (error) {
            Serial.print("deserializeJson() failed: ");
            Serial.println(error.c_str());
            return;
        }
        #endif
        const int stat = doc["payload"]["status"];          
        _data._request = stat;

        #ifdef DEBUG
            Serial.printf("\nRequest Status : %d\n", stat);
        #endif

        doc.clear();
        doc["_terminalTime"] = terminalTime;
        doc["_groupTag"]     = lantai;
        doc["version"]       = "1.5.0";
        char payload_callback[100];
        serializeJson(doc, payload_callback);            
        publish(_pub.ver.c_str(), payload_callback); 

    }

#endif
