#include <Arduino.h>
#include <WiFiManager.h>
#include "MultiFlock2.h"
//#include "MultiFlock.h"

Flock l1("Lantai1",&modbus);
Flock l2("Lantai2",&modbus);

xSemaphoreHandle  hand2 = NULL;
void flock12(void * param){
  while(1){
    l1.loop();
    // l2.loop();
    vTaskDelay(1);
  }
}
void flock1(void * pvParameters) {
  while(1) {
    if(portTICK_PERIOD_MS % 5000 == 0){
      l1.mqttConnect();
      // l1.Read();
      l1.NotifSend();
    }
  //if(xSemaphoreTake(hand1, 500)){
    // l1.loop();
    // xSemaphoreGive(hand2);
    vTaskDelay(1);
 // }
  }
}

void flock2(void * pvParameters) {
  while(1) {
    if(portTICK_PERIOD_MS % 4000 == 0){
      l2.mqttConnect();
      // l2.Read();
      l2.NotifSend();
    }
  // if(xSemaphoreTake(hand2, 2000)){
  // l2.loop();
  //xSemaphoreGive(hand1);
  vTaskDelay(1);
  // }
}
}
void setup() {
    modbusSetup();
    WiFi.begin("Chickin","joni1234");
    while (WiFi.status() != WL_CONNECTED);
    l1.setup(esp2);
    l2.setup(esp);
    l2.mqttConnect();
    l1.mqttConnect();
    // hand1 = xSemaphoreCreateBinary();
    hand2 = xSemaphoreCreateBinary();

    xTaskCreatePinnedToCore (
      flock1,     // Function to implement the task
      "flock1",   // Name of the task
      10000,      // Stack size in words
      NULL,      // Task input parameter
      1,        // Priority of the task
      NULL,
      1
    );

    // xTaskCreatePinnedToCore (
    //   flock2,     // Function to implement the task
    //   "flock2",   // Name of the task
    //   10000,      // Stack size in words
    //   NULL,      // Task input parameter
    //   1,        // Priority of the task
    //   NULL,
    //   1
    // );

    xTaskCreatePinnedToCore (
      flock12,     // Function to implement the task
      "flock12",   // Name of the task
      10000,      // Stack size in words
      NULL,      // Task input parameter
      1,        // Priority of the task
      NULL,
      1
    );
    // xSemaphoreGive(hand1);
}
// Flock fm(2, &modbus);

// void setup() {
//   modbusSetup();
//   WiFi.begin("Chickin","joni1234");
//   while (WiFi.status() != WL_CONNECTED);
//   fm.setup(esp);

// }

void loop() {
  // fm.mqttConnect();
  // fm.Read();
  // fm.loop();
  // l1.mqttConnect();
  // l1.Read();
  // l1.NotifSend();
  // l1.loop();
  // l2.Read();
  // l2.NotifSend();
} 

